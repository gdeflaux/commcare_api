# CommcareApi

A CommCare API wrapper in Ruby.

## Installation

Add this line to your application's Gemfile:

    gem 'commcare_api'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install commcare_api

## Supported APIs

* [List Cases](https://wiki.commcarehq.org/display/commcarepublic/List+Cases): `get_cases`
* [Case Data](https://wiki.commcarehq.org/display/commcarepublic/Case+Data): `get_case`
* [List Forms](https://wiki.commcarehq.org/display/commcarepublic/Form+Data): `get_forms`
* [Form Data](https://wiki.commcarehq.org/display/commcarepublic/Form+Data): `get_form`
* [List Groups](https://wiki.commcarehq.org/display/commcarepublic/List+Groups): `get_groups`
* [List Mobile Workers](https://wiki.commcarehq.org/display/commcarepublic/List+Mobile+Workers): `get_mobile_workers`, `get_mobile_worker`
* [List Web Users](https://wiki.commcarehq.org/display/commcarepublic/List+Web+Users): `get_web_users`, `get_web_user`
* [Data Forwarding (GET only)](https://wiki.commcarehq.org/display/commcarepublic/Data+Forwarding): `get_data_forwarding`
* [Application Structure](https://wiki.commcarehq.org/display/commcarepublic/Application+Structure+API): `get_application_structure`
* [Fixture Data (GET only)](https://wiki.commcarehq.org/display/commcarepublic/Fixture+Data): `get_fixtures`, `get_fixture`, `get_fixture_item`

## Usage

    require 'commcare_api'

    ccc = CommcareApi::CommcareConnector.new(username, password)
    response = ccc.get_cases("my_domain", type: "my_case_type", date_modified_start: "2014-05-30", limit: 15)
    puts response.body

Filters are added to the request with an options hash as shown above. See [CommCare documentation](https://wiki.commcarehq.org/display/commcarepublic/CommCare+HQ+APIs) for possible filters.

## Getting data in multiple queries

If the total amount of data you want cannot be retrieved with a single query you will have to perform multiple queries.

    require 'commcare_api'

    ccc = CommcareApi::CommcareConnector.new(username, password)
    response = ccc.get_cases("my_domain", type: "my_case_type", date_modified_start: "2014-05-30", limit: 15)

    while !response.nil? do
      # Get the data you need
      response = ccc.get_next_data
    end

You can also use the `get_previous_data` method similarly.

## Contributing

1. Fork it (http://github.com/gdeflaux/commcare_api/fork)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
