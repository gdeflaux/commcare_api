# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'commcare_api/version'

Gem::Specification.new do |spec|
  spec.name          = "commcare_api"
  spec.version       = CommcareApi::VERSION
  spec.authors       = ["Guillaume Deflaux"]
  spec.email         = ["deflaux.guillaume@gmail.com"]
  spec.summary       = "A CommCare API wrapper in Ruby"
  spec.description   = "A CommCare API wrapper in Ruby"
  spec.homepage      = "https://github.com/gdeflaux/commcare_api"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency 'rspec'
  spec.add_dependency "httpi"
  spec.add_dependency "httpclient"
  spec.add_dependency "json"
end
