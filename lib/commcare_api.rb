require "commcare_api/version"
require "httpi"
require "httpclient"
require "json"

module CommcareApi
  
  CC_BASE_URL = "https://www.commcarehq.org/a"
  
  class CommcareConnector
    
    def initialize(user, password, version="0.5")
      @user = user
      @password = password
      @version = "v#{version}"
      @last_response = nil
      @last_request = nil
    end
  
    def get_next_data
      get_contiguous_data("next")
    end
    
    def get_previous_data
      get_contiguous_data("previous")
    end
  
    def get_cases(domain, options = {})
      url = build_url(domain, "case", options)
      get_request(url)
    end
  
    def get_case(domain, case_id, options = {})
      url = build_url(domain, "case/#{case_id}", options)
      get_request(url)
    end
  
    def get_forms(domain, options = {})
      url = build_url(domain, "form", options)
      get_request(url)
    end
  
    def get_form(domain, form_id, options = {})
      url = build_url(domain, "form/#{form_id}", options)
      get_request(url)
    end
    
    def get_groups(domain, options = {})
      url = build_url(domain, "group", options)
      get_request(url)
    end
    
    def get_mobile_workers(domain, options = {})
      url = build_url(domain, "user", options)
      get_request(url)
    end
  
    def get_mobile_worker(domain, user_id, options = {})
      url = build_url(domain, "user/#{user_id}", options)
      get_request(url)
    end
    
    def get_web_users(domain)
      url = build_url(domain, "web-user", {})
      get_request(url)
    end
  
    def get_web_user(domain, user_id)
      url = build_url(domain, "web-user/#{user_id}", {})
      get_request(url)
    end
    
    def get_application_structure(domain)
      url = build_url(domain, "application", {})
      get_request(url)
    end
    
    def get_data_forwarding(domain)
      url = build_url(domain, "data-forwarding", {})
      get_request(url)
    end
    
    def get_fixtures(domain)
      url = build_url(domain, "fixture", {})
      get_request(url)
    end
    
    def get_fixture(domain, type)
      url = build_url(domain, "fixture", {fixture_type: type})
      get_request(url)
    end
  
    def get_fixture_item(domain, item_id)
      url = build_url(domain, "fixture/#{item_id}", {})
      get_request(url)
    end
  
    protected

    def get_request(url)
      request = HTTPI::Request.new(url)
      request.auth.digest(@user, @password)
      response = HTTPI.get(request, :httpclient)
      @last_response = response
      @last_request = request
      response
    end

    def build_url(domain, action, options)
      url = "#{CommcareApi::CC_BASE_URL}/#{domain}/api/#{@version}/#{action}/"
      options = options.map {|k,v| "#{k.to_s}=#{v}"}.join("&")
      url = "#{url}?#{options}" if !options.empty?
      url
    end
    
    def get_contiguous_data(direction)
      return nil if @last_response.nil?
      j = JSON.parse(@last_response.body)
      return nil if j["meta"][direction].nil?
      @last_request.url.query = j["meta"][direction].gsub(/\?/, "")
      url = @last_request.url.to_s
      get_request(url)
    end
    
  end # Class
  
end # Module
