# The commcare domain must have a least 2 cases

require 'spec_helper'

describe CommcareApi do
  ccc = CommcareApi::CommcareConnector.new(ENV["CC_USER"], ENV["CC_PWD"])
  
  r = ccc.get_cases(ENV["CC_DOMAIN"], offset: 100, limit: 1)
  puts r.code
  j = JSON.parse(r.body)

  puts JSON.pretty_generate(j)
  
  it "get_next_data (nil)" do
    r = ccc.get_next_data
    expect(r.class).to eq nil.class
  end
  
  it "get_previous_data (nil)" do
    r = ccc.get_previous_data
    expect(r.class).to eq nil.class
  end
  
  case_id = ""
  it "get_cases" do
    r = ccc.get_cases(ENV["CC_DOMAIN"])
    case_id = JSON.parse(r.body)["objects"][0]["case_id"]
    expect(r.code).to eq 200
  end
  
  it "get_case" do
    r = ccc.get_case(ENV["CC_DOMAIN"], case_id)
    expect(r.code).to eq 200
  end
  
  form_id = ""
  it "get_forms" do
    r = ccc.get_forms(ENV["CC_DOMAIN"])
    form_id = JSON.parse(r.body)["objects"][0]["id"]
    expect(r.code).to eq 200
  end
  
  it "get_form" do
    r = ccc.get_form(ENV["CC_DOMAIN"], form_id)
    expect(r.code).to eq 200
  end
  
  it "get_previous_data" do
    r = ccc.get_cases(ENV["CC_DOMAIN"], limit: 1)
    r = ccc.get_next_data
    expect(r.code).to eq 200
  end
  
  it "get_previous_data" do
    r = ccc.get_cases(ENV["CC_DOMAIN"], limit: 1, offset: 1)
    r = ccc.get_previous_data
    expect(r.code).to eq 200
  end
  
  it "get_groups" do
    r = ccc.get_groups(ENV["CC_DOMAIN"])
    expect(r.code).to eq 200
  end
  
  mobile_worker_id = ""
  it "get_mobile_workers" do
    r = ccc.get_mobile_workers(ENV["CC_DOMAIN"])
    mobile_worker_id = JSON.parse(r.body)["objects"][0]["id"]
    expect(r.code).to eq 200
  end

  it "get_mobile_worker" do
    r = ccc.get_mobile_worker(ENV["CC_DOMAIN"], mobile_worker_id)
    expect(r.code).to eq 200
  end
  
  web_user_id = ""
  it "get_web_users" do
    r = ccc.get_web_users(ENV["CC_DOMAIN"])
    web_user_id = JSON.parse(r.body)["objects"][0]["id"]
    expect(r.code).to eq 200
  end
  
  it "get_web_user" do
    r = ccc.get_web_user(ENV["CC_DOMAIN"], web_user_id)
    expect(r.code).to eq 200
  end
  
  it "get_application_structure" do
    r = ccc.get_application_structure(ENV["CC_DOMAIN"])
    expect(r.code).to eq 200
  end
  
  it "get_data_forwarding" do
    r = ccc.get_data_forwarding(ENV["CC_DOMAIN"])
    expect(r.code).to eq 200
  end
  
  fixture_type = ""
  fixture_item_id = ""
  it "get_fixtures" do
    r = ccc.get_fixtures(ENV["CC_DOMAIN"])
    fixture_type = JSON.parse(r.body)["objects"][0]["fixture_type"]
    fixture_item_id= JSON.parse(r.body)["objects"][0]["id"]
    expect(r.code).to eq 200
  end
  
  it "get_fixture" do
    r = ccc.get_fixture(ENV["CC_DOMAIN"], fixture_type)
    expect(r.code).to eq 200
  end

  it "get_fixture_item" do
    r = ccc.get_fixture_item(ENV["CC_DOMAIN"], fixture_item_id)
    expect(r.code).to eq 200
  end

end